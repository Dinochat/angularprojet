import { Component, OnInit } from '@angular/core';
import { JexiaService } from '../services/jexia.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-modifier-game',
  templateUrl: './modifier-game.component.html',
  styleUrls: ['./modifier-game.component.css']
})
export class ModifierGameComponent implements OnInit {

 
  gameForm = new FormGroup({
    name: new FormControl(),
    editor: new FormControl(),
    pegi: new FormControl()
});

gameUpdtadeForm = new FormGroup({
  newName: new FormControl(),
  newEditor: new FormControl(),
  newPegi: new FormControl()
}); 

  videoGameDataSet;
  videoGames= []; 

  constructor(private jexia: JexiaService) {
   }

  ngOnInit(): void {
    console.log(this.gameForm.value);
    this.videoGameDataSet= this.jexia.dataModule.dataset('Videogame');
  }

updateGame(){
  const posts = this.jexia.dataModule.dataset("Videogame");
  posts
    .update({ name:  this.gameForm.value.name })
    .where(field => field("Name").isInArray(this.gameForm.value.name))
    .subscribe(affectedRecords => {
      [{ Name: this.gameUpdtadeForm.value.newName, Editor: this.gameUpdtadeForm.value.newEditor, Pegi: this.gameUpdtadeForm.value.newPegi }]
    });
  }
 
}
 