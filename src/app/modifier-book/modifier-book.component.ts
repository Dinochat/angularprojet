import { Component, OnInit } from '@angular/core';
import { JexiaService } from '../services/jexia.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-modifier-book',
  templateUrl: './modifier-book.component.html',
  styleUrls: ['./modifier-book.component.css']
})
export class ModifierBookComponent implements OnInit {

  bookForm = new FormGroup({
    title: new FormControl(),
    author: new FormControl(),
    date: new FormControl()
});

bookUpdtadeForm = new FormGroup({
  newTitle: new FormControl(),
  newAuthor: new FormControl(),
  newDate: new FormControl()
});

  bookDataSet;
  book= []; 

  constructor(private jexia: JexiaService) {
   }

  ngOnInit(): void {
    console.log(this.bookForm.value);
    this.bookDataSet= this.jexia.dataModule.dataset('Book');
  }

updateBook(){
  const posts = this.jexia.dataModule.dataset("Book");
 
  posts
    .update({ name:  this.bookForm.value.name })
    .where(field => field("Title").isInArray(this.bookForm.value.name))
    .subscribe(affectedRecords => {
      [{ Title: this.bookUpdtadeForm.value.newTitle, Author: this.bookUpdtadeForm.value.newAuthor, PublicationDate: this.bookUpdtadeForm.value.newDate }]
    });
  }
 
}
 