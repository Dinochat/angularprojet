import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierBookComponent } from './modifier-book.component';

describe('ModifierBookComponent', () => {
  let component: ModifierBookComponent;
  let fixture: ComponentFixture<ModifierBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
