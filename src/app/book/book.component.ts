import { Component, OnInit } from '@angular/core';
import { JexiaService } from '../services/jexia.service';
import { FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {


  bookForm = new FormGroup({
    Title: new FormControl(), 
    Author: new FormControl(),
    PublicationDate: new FormControl()
});

bookDataSet;
book= []; 

constructor(private jexia: JexiaService) {
 }

ngOnInit(): void {
  console.log(this.bookForm.value);
  this.bookDataSet= this.jexia.dataModule.dataset('Book');
  this.loadBook();
}

loadBook() {
    this.bookDataSet.select().subscribe(records => {
            this.book = records;
        },
        error => {
            console.log('error');
        });
}
}
