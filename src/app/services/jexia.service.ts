import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {dataOperations as JexiaDataOperations, jexiaClient} from 'jexia-sdk-js/browser';


@Injectable({
  providedIn: 'root'
})
export class JexiaService {

  credentials = {
    projectID: environment.jexia.projectID,
    key: environment.jexia.key,
    secret: environment.jexia.secret,
  };

  dataModule = JexiaDataOperations();
  maData = []

  constructor() {
    jexiaClient().init(this.credentials, this.dataModule);
  }

  getVideogame() {

   this.dataModule.dataset('Videogame').select().subscribe(data=> {
      this.maData= data;
  },
  error => {
      console.log('error');
  });

  return this.maData;
  }

  insertGame(name:string, editor:string, pegi: number){
    const posts = this.dataModule.dataset("Videogame");
    const insertQuery = posts.insert({
      Name: name,
      Editor: editor,
      Pegi: pegi
     }).execute().then(data => console.log(data));
  }

  deleteGame(){
    const posts = this.dataModule.dataset("Videogame");
    posts
      .delete()
      .where(field => field("Name").isLike("%test%"))
      .subscribe(deletedFields => {
        // you will be able to access the deleted posts here
        // they are not stored in the DB anymore, but maybe you
        // want to display a visual confirmation of what was deleted
      });
  }

updateGame(name:string){
  const posts = this.dataModule.dataset("Videogame");

  posts
    .update({ name: "Fxck" })
    .where(field => field("Name").isInArray(["Fxck"]))
    .subscribe(affectedRecords => {
     /* [{ title: "Same title for tom and harry posts", author_name: "Tom" },
         { title: "Same title for tom and harry posts", author_name: "Tom" },
         { title: "Same title for tom and harry posts", author_name: "Harry" },
         { title: "Same title for tom and harry posts", author_name: "Tom" }]
      */
    });
}

  
}
