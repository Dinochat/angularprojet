import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookComponent } from '../book/book.component';
import { VideogameComponent } from '../videogame/videogame.component';
import { IndexJeuxComponent } from '../index-jeux/index-jeux.component';
import { SupprimerJeuxComponent } from '../supprimer-jeux/supprimer-jeux.component';
import { IndexComponent } from '../index/index.component';
import { SupprimerbookComponent } from '../supprimerbook/supprimerbook.component';
import { BookAddComponent } from '../book-add/book-add.component';
import { ModifierBookComponent } from '../modifier-book/modifier-book.component';
import { ModifierGameComponent } from '../modifier-game/modifier-game.component';

const routes: Routes = [
    {  path: '', 
        component: IndexComponent, 
        pathMatch: 'full' },
    {
        path: 'book',
        component: BookComponent,
    },
    {
        path: 'videogame',
        component: VideogameComponent,
    },
    {
        path: 'indexjeuxvideo',
        component: IndexJeuxComponent,
    },
    {
        path: 'supprimerjeux',
        component: SupprimerJeuxComponent,
    },
    {
        path: 'supprimerbook',
        component: SupprimerbookComponent,
    },
    {
        path: 'bookAdd',
        component: BookAddComponent,
    },
    {
        path: 'updatebook',
        component: ModifierBookComponent,
    },
    {
        path: 'updateGame',
        component: ModifierGameComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }