import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexJeuxComponent } from './index-jeux.component';

describe('IndexJeuxComponent', () => {
  let component: IndexJeuxComponent;
  let fixture: ComponentFixture<IndexJeuxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexJeuxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexJeuxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
