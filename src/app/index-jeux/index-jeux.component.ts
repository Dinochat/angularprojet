import { Component, OnInit } from '@angular/core';
import { JexiaService } from '../services/jexia.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-index-jeux',
  templateUrl: './index-jeux.component.html',
  styleUrls: ['./index-jeux.component.css']
})
export class IndexJeuxComponent implements OnInit {

  gameForm = new FormGroup({
    name: new FormControl(), 
    editor: new FormControl(),
    pegi: new FormControl()
});

videoGameDataSet;
videoGames= []; 

constructor(private jexia: JexiaService) {
 }

ngOnInit(): void {
  console.log(this.gameForm.value);
  this.videoGameDataSet= this.jexia.dataModule.dataset('Videogame');
  this.loadVideoGames();
}

loadVideoGames() {
    this.videoGameDataSet.select().subscribe(records => {
            this.videoGames = records;
        },
        error => {
            console.log('error');
        });
}
}
