import { Component, OnInit } from '@angular/core';
import { JexiaService } from '../services/jexia.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-supprimer-jeux',
  templateUrl: './supprimer-jeux.component.html',
  styleUrls: ['./supprimer-jeux.component.css']
})

export class SupprimerJeuxComponent implements OnInit {

  gameSuppForm = new FormGroup({
    name: new FormControl(),
    editor: new FormControl(),
    pegi: new FormControl()
});

  videoGameDataSet;
  videoGames= []; 

  constructor(private jexia: JexiaService) {
   }

  ngOnInit(): void {
    console.log(this.gameSuppForm.value);
    this.videoGameDataSet= this.jexia.dataModule.dataset('Videogame');
  }

  supprimeGame() {
      const posts = this.jexia.dataModule.dataset("Videogame");
      const insertQuery = posts.delete()
        .where(field => field("Name").isLike(this.gameSuppForm.value.name))
        .execute().then(data => console.log(data));
  }

}
