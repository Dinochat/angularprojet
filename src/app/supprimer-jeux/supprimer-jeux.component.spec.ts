import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimerJeuxComponent } from './supprimer-jeux.component';

describe('SupprimerJeuxComponent', () => {
  let component: SupprimerJeuxComponent;
  let fixture: ComponentFixture<SupprimerJeuxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupprimerJeuxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimerJeuxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
