import { Component, OnInit } from '@angular/core';
import { JexiaService } from '../services/jexia.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-videogame',
  templateUrl: './videogame.component.html',
  styleUrls: ['./videogame.component.css']
}) 
export class VideogameComponent implements OnInit {

  gameForm = new FormGroup({
    name: new FormControl(),
    editor: new FormControl(),
    pegi: new FormControl()
});

  videoGameDataSet;
  videoGames= []; 

  constructor(private jexia: JexiaService) {
   }

  ngOnInit(): void {
    console.log(this.gameForm.value);
    this.videoGameDataSet= this.jexia.dataModule.dataset('Videogame');
  }
  
subGame() {
  const posts = this.jexia.dataModule.dataset("Videogame");
    const insertQuery = posts.insert({
      Name: this.gameForm.value.name,
      Editor: this.gameForm.value.editor,
      Pegi: this.gameForm.value.pegi
     }).execute().then(data => console.log(data));
}

}
