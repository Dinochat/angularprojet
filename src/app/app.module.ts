import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderTitleComponent } from './header-title/header-title.component';
import { ParticlesComponent } from './particles/particles.component';
import { BookComponent } from './book/book.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { VideogameComponent } from './videogame/videogame.component';
import { IndexJeuxComponent } from './index-jeux/index-jeux.component';
import { SupprimerJeuxComponent } from './supprimer-jeux/supprimer-jeux.component';
import { IndexComponent } from './index/index.component';
import { BookAddComponent } from './book-add/book-add.component';
import { SupprimerbookComponent } from './supprimerbook/supprimerbook.component';
import { ModifierBookComponent } from './modifier-book/modifier-book.component';
import { ModifierGameComponent } from './modifier-game/modifier-game.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderTitleComponent,
    ParticlesComponent,
    BookComponent,
    VideogameComponent,
    IndexJeuxComponent,
    SupprimerJeuxComponent,
    IndexComponent,
    BookAddComponent,
    SupprimerbookComponent,
    ModifierBookComponent,
    ModifierGameComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
