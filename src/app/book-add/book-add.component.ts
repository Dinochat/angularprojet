import { Component, OnInit } from '@angular/core';
import { JexiaService } from '../services/jexia.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-book-add',
  templateUrl: './book-add.component.html',
  styleUrls: ['./book-add.component.css']
})
export class BookAddComponent implements OnInit {

  bookForm = new FormGroup({
    title: new FormControl(),
    author: new FormControl(),
    publicationDate: new FormControl()
});

  bookDataSet;
  book= []; 

  constructor(private jexia: JexiaService) {
   }

  ngOnInit(): void {
    console.log(this.bookForm.value);
    this.bookDataSet= this.jexia.dataModule.dataset('Book');
  }
  
addBook() {
  const posts = this.jexia.dataModule.dataset("Book");
    const insertQuery = posts.insert({
      Title: this.bookForm.value.title,
      Author: this.bookForm.value.author,
      PublicationDate: this.bookForm.value.publicationDate
     }).execute().then(data => console.log(data));
}

}
