import { Component, OnInit } from '@angular/core';
import { JexiaService } from '../services/jexia.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-supprimerbook',
  templateUrl: './supprimerbook.component.html',
  styleUrls: ['./supprimerbook.component.css']
})
export class SupprimerbookComponent implements OnInit {

  bookSuppForm = new FormGroup({
    title: new FormControl(),
    author: new FormControl(),
    date: new FormControl()
});

  bookDataSet;
  book= []; 

  constructor(private jexia: JexiaService) {
   }

  ngOnInit(): void {
    console.log(this.bookSuppForm.value);
    this.bookDataSet= this.jexia.dataModule.dataset('Book');
  }

  supprimeBook() {
      const posts = this.jexia.dataModule.dataset("Book");
      const insertQuery = posts.delete()
        .where(field => field("Title").isLike(this.bookSuppForm.value.title))
        .execute().then(data => console.log(data));
  }

}
