import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimerbookComponent } from './supprimerbook.component';

describe('SupprimerbookComponent', () => {
  let component: SupprimerbookComponent;
  let fixture: ComponentFixture<SupprimerbookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupprimerbookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimerbookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
